package com.example.kevin.kb.DatosPlatillos;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kevin.kb.R;

import java.util.ArrayList;
import java.util.List;

public class adapter_platillos extends RecyclerView.Adapter<adapter_platillos.Mivista> {

    Context context;
    List<Datos_platillos> lista_platillos;

    public adapter_platillos(ArrayList<Datos_platillos> listadatosplatillos) {
    }

    public adapter_platillos(Context context, List<Datos_platillos> lista_platillos) {
        this.context = context;
        this.lista_platillos = lista_platillos;
    }

    @NonNull
    @Override
    public Mivista onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.menu_principal, viewGroup, false);


        return new Mivista(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Mivista mivista, int i) {

        mivista.titulo.setText(lista_platillos.get(i).getTitulo());
        mivista.descripcion.setText(lista_platillos.get(i).getDescripcion());
        mivista.precio.setText(lista_platillos.get(i).getPrecio());
        mivista.imagen.setImageResource(lista_platillos.get(i).getImg());
    }

    @Override
    public int getItemCount() {
        return lista_platillos.size();
    }

    public class Mivista extends RecyclerView.ViewHolder {

        TextView titulo, descripcion, precio;
        ImageView imagen;


        public Mivista(@NonNull View itemView) {
            super(itemView);

            titulo = itemView.findViewById(R.id.txt_foodname);
            descripcion = itemView.findViewById(R.id.txt_descripcionComida);
            precio = itemView.findViewById(R.id.preciocomida);
            imagen = itemView.findViewById(R.id.imagenfood);

        }
    }
}
