package com.example.kevin.kb;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.kevin.kb.Fragments.AboutUs;
import com.example.kevin.kb.Fragments.Buscar;
import com.example.kevin.kb.Fragments.Buscar_mapa;
import com.example.kevin.kb.Fragments.Calificaciones;
import com.example.kevin.kb.Fragments.Cocina;
import com.example.kevin.kb.Fragments.Favoritos;
import com.example.kevin.kb.Fragments.Mis_ordenes;
import com.example.kevin.kb.Fragments.Terminos_condiciones;

public class PrincipalMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.icono_carreta);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ///para agregar un fragment por default que se muestre al inicio de la aplicacion.
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.contenedorPrincipal, new Buscar()).commit();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        //creamos un FragmentManager
        FragmentManager fragmentManager = getSupportFragmentManager();


        if (id == R.id.nav_busca_restaurante) {
            //llamamos al fragment y reemplazamos el contenedorprincipal por el nuevo.
            fragmentManager.beginTransaction().replace(R.id.contenedorPrincipal, new Buscar()).commit();

        } else if (id == R.id.nav_search_in_map) {
            fragmentManager.beginTransaction().replace(R.id.contenedorPrincipal, new Buscar_mapa()).commit();
        } else if (id == R.id.nav_rest_mas_calif) {
            fragmentManager.beginTransaction().replace(R.id.contenedorPrincipal, new Calificaciones()).commit();
        } else if (id == R.id.nav_cocina) {
            fragmentManager.beginTransaction().replace(R.id.contenedorPrincipal, new Cocina()).commit();
        } else if (id == R.id.nav_favoritos) {
            fragmentManager.beginTransaction().replace(R.id.contenedorPrincipal, new Favoritos()).commit();
        } else if (id == R.id.nav_mis_ordenes) {
            fragmentManager.beginTransaction().replace(R.id.contenedorPrincipal, new Mis_ordenes()).commit();
        } else if (id == R.id.nav_compartir) {
            Toast.makeText(this, "Compartir", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_terminos) {
            fragmentManager.beginTransaction().replace(R.id.contenedorPrincipal, new Terminos_condiciones()).commit();
        } else if (id == R.id.nav_about) {
            fragmentManager.beginTransaction().replace(R.id.contenedorPrincipal, new AboutUs()).commit();
        } else if (id == R.id.nav_salir) {
            Toast.makeText(this, "Good Bye!!", Toast.LENGTH_SHORT).show();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
